﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARobot.Models
{
    class CommandHandler
    {
        private CommandHandler()
        {
            CommandQueue = new Queue<Command>();
            AvailableCommands = new List<Command>();
            FillCommandsList();
            inputable = true;
        }

        public void Process()
        {
            if (inputable)
            {
                while (inputable) HandleInput();
            }
            else
            {
                if (CommandQueue.Count != 0)
                    ExecuteNextCommand();
                else inputable = true;
            }
        }
        private void HandleInput()
        {
            Console.Clear();

            GameLoop.GetInstance().map.Print();
            PrintCommandQueue();
            PrintAvailableCommands();
            Console.WriteLine("\t0 - End editig");
            var comm = SelectCommand();
            if (comm!=null)CommandQueue.Enqueue(comm);
        }
        private void ExecuteNextCommand()
        {
            GameLoop.GetInstance().map.Print();
            var comm = CommandQueue.Dequeue();
            Console.WriteLine(comm.Info());
            if(!comm.Execute())GameLoop.GetInstance().Restart();
        }

        private Command SelectCommand()
        {
            var s = Console.ReadLine();
            char input = ' ';
            if (s != "") input = char.Parse(s);
            switch (input)
            {
                case '1': return new MoveCommand();
                case '2': return new RotateLeftCommannd();
                case '3': return new RotateRightCommand();
                case '4': return new FlipCommand();
                case '5': return ConstructLoop(); ;
                case '6': return new ActivateCommand();
                case '7': return new JumpCommand();
                case '8': return new GoDownCommand();
                case '0':
                    inputable = false;break;
                default:
                {
                    Console.WriteLine("Wrong input. Try again");
                    SelectCommand();
                }
                    break;
            }
            return null;
        }
        private LoopCommand ConstructLoop()
        {
            Command comand;
            int times=0;
            Console.WriteLine("Select commannd to insert in loop: ");
            PrintAvailableCommands();
            comand = SelectCommand();
            Console.WriteLine("Insert times to repeat");
            times = int.Parse(Console.ReadLine());
            return new LoopCommand(comand,times);

        }

        private void PrintAvailableCommands()
        {
            Console.WriteLine("List of available commands:");
            for (int i = 0; i < AvailableCommands.Count; i++)
            {
                Console.WriteLine("\t{0} - {1}",i+1,AvailableCommands[i].Info());
            }
        }

        public void PrintCommandQueue()
        {
            if(CommandQueue.Count==0)Console.WriteLine("Queue is empty");
            else
            {
                Console.WriteLine("Queue:");
                foreach (var command in CommandQueue)
                {
                    Console.WriteLine(command.Info());
                }
            }
        }
        private void FillCommandsList()
        {
            AvailableCommands.Add(new MoveCommand());
            AvailableCommands.Add(new RotateLeftCommannd());
            AvailableCommands.Add(new RotateRightCommand());
            AvailableCommands.Add(new FlipCommand());
            AvailableCommands.Add(new LoopCommand());
            AvailableCommands.Add(new ActivateCommand());
            AvailableCommands.Add(new JumpCommand());
            AvailableCommands.Add(new GoDownCommand());
        }
        public static CommandHandler GetInstance()
        {
            return instance ?? (instance = new CommandHandler());
        }

        private bool interactive;
        private bool inputable;
        private static CommandHandler instance;
        public Queue<Command> CommandQueue { get; set; }

        public List<Command>AvailableCommands { get; set; }
    }
}
