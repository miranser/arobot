﻿using System;
using System.Linq;
using System.Threading;
using ARobot.Models;

namespace ARobot
{
    public class GameLoop
    {
        private static GameLoop instance;
        private CommandHandler commandHandler;
        private bool interactive;
        private bool isRiched;
        private Robot robot;
        public Map map { get; set; }

        public void Initialize()
        {
            robot = Robot.GetInstance();
            map = new Map(robot);
            commandHandler = CommandHandler.GetInstance();
            robot.onInteractive += Robot_onInteractive;
            robot.onNonInteractive += Robot_onNonInteractive;
        }

        private void Robot_onInteractive()
        {
            interactive = true;
        }

        private void Robot_onNonInteractive()
        {
            interactive = false;
        }

        public void Start()
        {
            while (true)
            {
                Thread.Sleep(500);
                commandHandler.Process();
                CheckRequirements();
                if (isRiched) break;
            }
        }

        public void Restart()
        {
            map.Reset();
        }

        private void PrintInfo()
        {
            Console.WriteLine("Player's Cube:");
            robot.Info();
            robot.PositionCube.ObjectOnCube?.Info();
        }

        private void CheckRequirements()
        {
            var parasites = map.Interactives.Where(a => a.GetType() == "Parasite");
            var state = true;
            foreach (var parasite in parasites)
                if (!parasite.IsActive) state = false;
            isRiched = state;
        }

        public static GameLoop GetInstance()
        {
            return instance ?? (instance = new GameLoop());
        }
    }
}