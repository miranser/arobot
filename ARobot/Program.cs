﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARobot
{
    class Program
    {
        static void Main(string[] args)
        {
            GameLoop gl=GameLoop.GetInstance();
            gl.Initialize();
            gl.Start();
            Console.WriteLine("yeap. you made it. Again?(y/n)");
            string ans = Console.ReadLine();
            if(ans=="y"||ans=="Y")gl.Restart();
            Console.WriteLine("For exit press ANYKEY");

        }
    }
}
