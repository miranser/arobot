﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARobot
{
    public class Portal:InteractiveGameObject
    {
        public Portal(Cube positionCube)
        {
            this.Position = positionCube.Position;
            this.PositionCube = positionCube;
            Direction = new Direction();
        }

        

        public override bool Activate()
        {
            Teleport();
            return true;
        }
        private void Teleport()
        {
            Robot.GetInstance().PositionCube = LinkedPortal.PositionCube;
            //Robot.GetInstance().Flip();
        }

        public override string GetType()
        {
            return "Portal";
        }
        public Portal LinkedPortal { get; set; }
        
        public override void Info()
        {
            base.Info();
            //Console.WriteLine("\nLinked portal at:");
            //LinkedPortal.Info();
        }

    }
}
