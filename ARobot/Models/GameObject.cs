﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARobot
{
    public abstract class GameObject
    {
        public abstract void Info();
        public new abstract string GetType();
        public Point3D Position { get; set; }
        public string Name { get; set; }
    }
}