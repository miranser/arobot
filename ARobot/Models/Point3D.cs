﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARobot
{
    public class Point3D
    {
        public Point3D(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        Point3D(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Z = 0;
        }

        Point3D()
        {
            X = 0;
            Y = 0;
            Z = 0;
        }

        public Point3D Distance(Point3D firstPoint,Point3D secondPoint)
        {
            return new Point3D(Math.Abs(firstPoint.X-secondPoint.X),Math.Abs(firstPoint.Y-secondPoint.Y),Math.Abs(firstPoint.Z-secondPoint.Z));
        }

        public static bool operator ==(Point3D a, Point3D b)
        {
            if (a.X == b.X && a.Y == b.Y && a.Z == b.Z) return true;
            return false;
        }
        public static bool operator !=(Point3D a, Point3D b)
        {
            if (a.X == b.X && a.Y == b.Y && a.Z == b.Z) return false;
            return true;
        }
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
    }
}
