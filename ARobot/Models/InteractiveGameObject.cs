﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARobot
{
    public abstract class InteractiveGameObject:GameObject
    {
        public Cube PositionCube
        {
            get { return positionCube; }
            set
            {
                positionCube = value;
                Position = value.Position;
                //this.Position.Z += 1;
            }
        }
        public abstract bool Activate();

        public override void Info()
        {
            Console.WriteLine("Position:\n\t x:{0}\n\t y:{1}\n\t z:{2}", Position.X, Position.Y, Position.Z);
        }
        public bool IsActive { get; set; }
        public Direction Direction { get; set; }
        public ConsoleColor Color { get; set; }
        private Cube positionCube;
    }
}
