﻿using System;

namespace ARobot
{
    public class Robot:InteractiveGameObject
    {
        private Robot()
        {
            Direction = new Direction();
            Color = ConsoleColor.White;
        }
        public static Robot GetInstance()
        {
            return instance ?? (instance = new Robot());
        }

        
        public bool MoveForward()
        {
            if(PositionCube.GetNextCube(Direction.Value())!=null)
            {
                PositionCube = PositionCube.GetNextCube(Direction.Value());
                Position = PositionCube.Position;
            }
            else
            {
                return false;
            }
            if (PositionCube.ObjectOnCube != null) onInteractive();
            else onNonInteractive();
            return true;
        }

        public bool Jump()
        {
            if (PositionCube.GetNextUpperCube(Direction.Value()) != null)
            {
                PositionCube = PositionCube.GetNextUpperCube(Direction.Value());
                Position = PositionCube.Position;
            }
            else
            {
                return false;
            }
            return true;
        }
        public bool GoDown()
        {
            if (PositionCube.GetNextLowerCube(Direction.Value()) != null)
            {
                PositionCube = PositionCube.GetNextLowerCube(Direction.Value());
                Position = PositionCube.Position;
            }
            else
            {
                return false;
            }
            return true;
        }
        public bool RotateLeft()
        {
            Direction.Prev();
            return true;
        }

        public bool RotateRight()
        {
            Direction.Next();
            return true;
        }

        public bool Flip()
        {
            Direction.Flip();
            return true;
        }
        public override string GetType()
        {
            return "Robot";
        }
        
        private bool CheckForObstaclesToMove()
        {
            if (PositionCube.GetNextCube(Direction.Value()) == null&&PositionCube.LowerCube == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There is no way forward");
                Console.ResetColor();
                return false;
            }
            if (PositionCube.GetNextCube(Direction.Value()) == null && PositionCube.LowerCube != null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("There is no way forward.");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(" May be you'll try to go down?\n");
                Console.ResetColor();
                return false;
            }
            if (PositionCube.GetNextCube(Direction.Value()).FrontCube != null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("There is no way forward.");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(" May be you'll try to jump over?\n");
                Console.ResetColor();
                return false;
            }
            return true;
        }
        public override bool Activate()
        {
            if (PositionCube.ObjectOnCube != null)
            {
                PositionCube.ObjectOnCube.Activate();
                onNonInteractive();
                return true;
            }
            Console.WriteLine("\nNothing to activate\n");
            return false;
        }

        public override void Info()
        {
            base.Info();
            Direction.Info();
        }


        public delegate void Interactive();

        public delegate void NonInteractive();
        public event Interactive onInteractive;
        public event NonInteractive onNonInteractive;
        //public Direction.direction direction;
        private static Robot instance;

    }

}