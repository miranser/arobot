﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ARobot
{
    public class Map
    {
        public Map(Robot robot)
        {
            //Cubes = new Cube[Width,Length,Height];
            Interactives = new List<InteractiveGameObject>();
            Portals = new List<Portal>();
            InitialColor = 2;
            LoadMapFromFile();
            
        }
        private void LinkCubes()
        {
            for (var k = 0; k < Height; k++)
            for (var i = 0; i < Width; i++)
            for (var j = 0; j < Length; j++)
                if (Cubes[i, j, k] != null)
                {
                    Cubes[i, j, k].FrontCube = SetFrontCube(i, j, k);
                    Cubes[i, j, k].RearCube = SetRearCube(i, j, k);
                    Cubes[i, j, k].LeftCube = SetLeftCube(i, j, k);
                    Cubes[i, j, k].RightCube = SetRightCube(i, j, k);
                    Cubes[i, j, k].UpperCube = SetUpperCube(i, j, k);
                    Cubes[i, j, k].LowerCube = SetLowerCube(i, j, k);
                }
        }
        private void LoadMapFromFile()
        {
            
            var strings = File.ReadAllLines(@"map.txt");
            var header = strings.First().Split(' ');
            Length = short.Parse(header[1].Split(':').Last());
            Width = short.Parse(header.First().Split(':').Last());
            Height = short.Parse(header.Last().Split(':').Last());

            Cubes = new Cube[Length,Width,Height];

            var start = strings[1].Split(' ');
            StartX = short.Parse(start.First().Split(':').Last());
            StartY = short.Parse(start[1].Split(':').Last());
            StartZ = short.Parse(start.Last().Split(':').Last());
            CurrentLevel = StartZ;
            var charcube  = new char[Length,Width,Height];
            var currentString = 2;
            var level = 0;
            var len = 0;
            while (currentString!=strings.Length)
            {
                if (strings[currentString].Length < Length)
                {
                    level = int.Parse(strings[currentString]);
                    len = 0;
                }
                else
                {
                    var chars = strings[currentString].Split(' ');
                    for (var i = 0; i < Width; i++)
                    {
                        var c = char.Parse(chars[i]);
                        charcube[len, i, level] = c;
                        Cubes[len, i, level] = c == '.' ? null : new Cube(len, i, level);
                    }
                    len++;
                }
                currentString++; 
            }
            
            
            LinkCubes();
            FindInteractives(charcube);
            Robot.GetInstance().PositionCube = Cubes[StartX, StartY, StartZ];

        }
        private void FindInteractives(char[,,] charcube)
        {
            for (var k = 0; k < Height; k++)
            for (var i = 0; i < Length; i++)
            for (var j = 0; j < Width; j++)
                if (charcube[i, j, k] == '#')
                {
                    var paras = new Parasite(Cubes[i, j, k]);
                    Cubes[i, j, k].ObjectOnCube = paras;
                    paras.Color = ConsoleColor.Green;
                    Interactives.Add(paras);
                }
            var pchars = new List<char>();
            var portals = new List<Portal>();
            for (var k = 0; k < Height; k++)
            for (var i = 0; i < Length; i++)
            for (var j = 0; j < Width; j++)
                if (charcube[i, j, k] != '.' &&
                    charcube[i, j, k] != 'o' &&
                    charcube[i, j, k] != '#')
                {
                    var portal = new Portal(Cubes[i, j, k]);
                    pchars.Add(charcube[i, j, k]);
                    portals.Add(portal);
                    Cubes[i, j, k].ObjectOnCube = portal;
                    Interactives.Add(portal);
                }
            for (var i = 0; i < pchars.Count; i++)
                if (portals[i].LinkedPortal == null)
                {
                    var initial = pchars[i];
                    var linked = pchars.FindLastIndex(a => a == initial);

                    portals[i].LinkedPortal = portals[linked];
                    portals[linked].LinkedPortal = portals[i];
                    SetColor(portals[i]);
                }
        }
       private void SetColor(Portal portal)
        {
            var color = NextColor();
            portal.Color = color;
            portal.LinkedPortal.Color = color;
        }
        private ConsoleColor NextColor()
        {
            var colors = Enum.GetValues(typeof(ConsoleColor));
            InitialColor++;
            return (ConsoleColor)colors.GetValue(InitialColor);
        }
        private Cube SetLeftCube(int x, int y, int z)
        {
            if (x - 1 < 0) return null;
            return Cubes[x - 1, y, z];
        }
        private Cube SetRightCube(int x, int y, int z)
        {
            if (x + 1 > Width-1) return null;
            return Cubes[x + 1, y, z];
        }
        private Cube SetFrontCube(int x, int y, int z)
        {
            if (y + 1 > Length-1) return null;
            return Cubes[x, y + 1, z];
        }
        private Cube SetRearCube(int x, int y, int z)
        {
            if (y - 1 < 0) return null;
            return Cubes[x, y - 1, z];
        }
        private Cube SetUpperCube(int x, int y, int z)
        {
            if (z + 1 > Height -1) return null;
            return Cubes[x, y, z + 1];
        }
        private Cube SetLowerCube(int x, int y, int z)
        {
            if (z - 1 < 0) return null;
            return Cubes[x, y, z - 1];
        }
        public void Reset()
        {
            foreach (var parasite in Interactives)
            {
                parasite.IsActive = false;
                parasite.Color = ConsoleColor.Green;
            }
            Robot.GetInstance().PositionCube = Cubes[StartX, StartY, StartZ];
            Robot.GetInstance().Direction.Reset();
        }
        public Cube[,,] GetMap()
        {
            return Cubes;
        }

       public void Print()
        {
            Console.Clear();
            var level = Robot.GetInstance().PositionCube.Position.Z;
            for (var i = 0; i <Length; i++)
            {
                for (var j = 0; j < Width; j++)
                {
                    var r = CheckCubesForNull(i,j,level);
                    r = CheckCubesByZ(i, j, level,r);
                    if(r =='o')
                    {
                        r = SetCharForMap(Cubes[i, j, level].ObjectOnCube);
                        r = Cubes[i, j, level].Position == Robot.GetInstance().PositionCube.Position ? SetPlayerCharForMap() : r;
                    }
                    Console.Write("\t"+ r);
                    Console.ResetColor();
                }
                Console.WriteLine();
            }
        }
        private char CheckCubesForNull(int x,int y, int z)
        {
            var r = ' ';
            return Cubes[x, y, z] != null ? 'o' : r;
        }
        private char CheckCubesByZ(int x, int y, int z,char c)
        {
            if (Cubes[x, y, z] != null)
                if(Cubes[x,y,z].UpperCube!=null)return '/';
            if (Cubes[x, y, z] == null)
                if (z - 1 >= 0 && Cubes[x, y, z - 1] != null) return '.';
            return c;
        }
        private char SetPlayerCharForMap()
        {
            Console.ForegroundColor = ConsoleColor.White;
            switch (Robot.GetInstance().Direction.Value())
            {
                case 0: return '>';
                case 90: return 'v';
                case 180: return '<';
                case 270: return '^';
            }
            return '>';
        }
        private char SetCharForMap(InteractiveGameObject obj)
        {
            if (obj == null) return 'o';
            Console.ForegroundColor = obj.Color;
            if (obj.GetType() == "Portal") return '0';
            if (obj.GetType() == "Parasite") return '#';
            return 'o';
        }
        private short Width { get; set; } 
        private short Length { get; set; }
        private short Height { get; set; }
        private short StartX { get; set; }
        private short StartY { get; set; }
        private short StartZ { get; set; }
        private short CurrentLevel { get; set; }
        private int InitialColor { get; set; }
        public Cube[,,] Cubes { get; set; }
        public List<InteractiveGameObject> Interactives { get; }
        public List<Portal> Portals { get; }
    }
}