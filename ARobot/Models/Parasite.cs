﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARobot
{
    public class Parasite:InteractiveGameObject
    {
        public Parasite(Cube positionCube)
        {
            PositionCube = positionCube;
            Position = positionCube.Position;
        }
        public override bool Activate()
        {
            IsActive = !IsActive;
            Color = ConsoleColor.White;
            return true;
        }

        public override string GetType()
        {
            return "Parasite";
        }
        
    }
}
