﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARobot
{
    public class Cube:GameObject
    {
        public Cube()
        {}

        public Cube(int x, int y, int z)
        {
            Position = new Point3D(x,y,z);
        }
        public Cube GetNextCube(int direction)
        {
            Cube nextCube = null;
            if (CheckNextCube(direction) == null) return null;
            switch (direction)
            {
                case 0: nextCube = FrontCube.UpperCube==null?FrontCube:null;
                    break;
                case 90:
                    nextCube = RightCube.UpperCube == null ? RightCube : null;
                    break;
                case 180:
                    nextCube = RearCube.UpperCube == null ? RearCube : null;
                    break;
                case 270:
                    nextCube = LeftCube.UpperCube == null ? LeftCube : null;
                    break;
            }
            return nextCube;
        }

        private Cube CheckNextCube(int direction)
        {
            Cube nextCube = null;

            switch (direction)
            {
                case 0:
                    nextCube = FrontCube;
                    break;
                case 90:
                    nextCube = RightCube;
                    break;
                case 180:
                    nextCube = RearCube;
                    break;
                case 270:
                    nextCube = LeftCube;
                    break;
            }
            return nextCube;
        }
        public Cube GetNextUpperCube(int direction)
        {
            Cube nextCube = null;
            switch (direction)
            {
                case 0: nextCube = FrontCube.UpperCube;
                    break;
                case 90: nextCube = RightCube.UpperCube;
                    break;
                case 180: nextCube = RearCube.UpperCube;
                    break;
                case 270: nextCube = LeftCube.UpperCube;
                    break;
            }
            return nextCube;
        }
        public Cube GetNextLowerCube(int direction)
        {
            Cube nextCube = null;
            if (LowerCube == null) return null;
            switch (direction)
            {
                case 0:
                    nextCube = FrontCube == null ? LowerCube.FrontCube : null;
                    break;
                case 90:
                    nextCube = RightCube == null ? LowerCube.RightCube : null;
                    break;
                case 180:
                    nextCube = RearCube == null ? LowerCube.RearCube : null;
                    break;
                case 270:
                    nextCube = LeftCube == null ? LowerCube.LeftCube : null;
                    break;
            }
            return nextCube;
        }
        public override void Info()
        {
            {
                Console.WriteLine("Cube position:\n\t x:{0}\n\t y:{1}\n\t z:{2}", Position.X, Position.Y, Position.Z);
            }
            ObjectOnCube?.Info();
        }

        public override string GetType()
        {
            return "Cube";
        }

        public Cube RearCube { get; set; }
        public Cube LeftCube { get; set; }
        public Cube RightCube { get; set; }
        public Cube FrontCube { get; set; }
        public Cube UpperCube { get; set; }
        public Cube LowerCube { get; set; }
        public InteractiveGameObject ObjectOnCube { get; set; }
    }
}
