﻿using System;

namespace ARobot
{
    public abstract class Command
    {
        public abstract bool Execute();
        public abstract string Info();
    }

    public class MoveCommand : Command
    {
        public override bool Execute()
        {
            return Robot.GetInstance().MoveForward();
        }
        public override string Info()
        {
            return "Move forward";
        }
    }
    public class RotateLeftCommannd : Command
    {
        public override bool Execute()
        {
            return Robot.GetInstance().RotateLeft();
        }

        public override string Info()
        {
            return "Rotate left";
        }
    }
    public class RotateRightCommand : Command
    {
        public override bool Execute()
        {
            return Robot.GetInstance().RotateRight();
        }
        public override string Info()
        {
            return "Rotate right";
        }
    }
    public class ActivateCommand : Command
    {
        public override bool Execute()
        {
            return Robot.GetInstance().Activate();
        }
        public override string Info()
        {
            return "Activate";
        }
    }
    public class FlipCommand:Command
    {
        public override bool Execute()
        {
            return Robot.GetInstance().Flip();
        }
        public override string Info()
        {
            return "Flip";
        }
    }

    public class JumpCommand : Command
    {
        public override bool Execute()
        {
            return Robot.GetInstance().Jump();
        }

        public override string Info()
        {
            return "Jump";
        }
    }
    public class GoDownCommand : Command
    {
        public override bool Execute()
        {
            return Robot.GetInstance().GoDown();
        }

        public override string Info()
        {
            return "Go down";
        }
    }
    public class LoopCommand:Command
    {
        public LoopCommand(){}
        public LoopCommand(Command command, int times)
        {
            Times = times;
            Command = command;
        }
        public override bool Execute()
        {
            for (int i = 0; i < Times; i++)
            {
                if (!Command.Execute()) return false;
            }
            return true;
        }
        public override string Info()
        {
            return "Loop";
        }
        public int Times { get; set; }

        public Command Command { get; set; }
    }

}