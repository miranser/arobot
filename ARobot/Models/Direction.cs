﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARobot
{
    public class Direction
    {
        public Direction()
        {
            degree = new LinkedList<int>();
            FillList();
            currentNode = degree.First;
        }

        public Direction(int defailtDirection)
        {
            degree = new LinkedList<int>();
            FillList();
            currentNode = degree.Find(defailtDirection);
        }

        private void FillList()
        {
            degree.AddFirst(new LinkedListNode<int>(270));
            degree.AddFirst(new LinkedListNode<int>(180));
            degree.AddFirst(new LinkedListNode<int>(90));
            degree.AddFirst(new LinkedListNode<int>(0));

        }
        public int Next()
        {
            int value = currentNode.Next?.Value ?? 0;
            currentNode = currentNode.Next ?? degree.First;
            return value;

        }
        public int Prev()
        {
            int value = currentNode.Previous?.Value ?? 270;
            currentNode = currentNode.Previous ?? degree.Last;
            return value;

        }

        public int Flip()
        {
            Next();
            return Next();
        }

        public int Value()
        {
            return currentNode.Value;
        }

        public void Reset()
        {
            currentNode = degree.First;
        }
        public void Info()
        {
            Console.WriteLine("Direction: "+currentNode.Value);
        }
        private LinkedListNode<int> currentNode;
        private LinkedList<int> degree;
    }
}
